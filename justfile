# Default command is to list all commands.
@default:
  just --list

# Lint both the library and dependencies.
lint fix="": (lint-lib fix)  lint-deny

# Lint the Rust code.
lint-lib fix="":
  cargo clippy --all-targets --color always {{ if fix != "" { "--allow-dirty --allow-staged --fix" } else { "" } }}
  {{ if fix != "" { "cargo +nightly fmt" } else { "" } }}

# Do fixups where we can.
fix: (lint-lib "y")

# Lint project dependencies and licenses
lint-deny:
  cargo deny check

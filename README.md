# Ratio Color

Color palette library.

## Changelog

This repository keeps a [CHANGELOG.md](./CHANGELOG.md) according to the recommendations by
[Keep a Changelog](https://keepachangelog.com/).

## Contributions and license

To get contributing, feel free to fork, pick up an issue or file your own and get going for your
first merge! We'll be more than happy to help.

You might have noticed that this crate follows the "MIT-OR-Apache-2.0" license and some of our other
crates follow the "GPL-3.0-or-later" license. This is intentional. Some crates, we consider as "just
utilities" that we would like to offer to the entire Rust community without any question asked or
guarantees given. This is such a utility crate.

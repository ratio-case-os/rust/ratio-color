//! A library to make palette management for plotting projects easier. It is built on the crate
//! [`palette`] that allows for creating, mixing and generally working with colors and pixels.
//!
//! A short categorical example utilizing the Bold preset:
//!
//! ```rust
//! use ratio_color::{Categorical, Container, Palette, key_to_order};
//! use palette::Srgba;
//! let keys = ["foo", "bar", "baz", "quux"];
//! let categorical: Palette<&str, Srgba<u8>> = Palette::with_preset(
//!     &Categorical::Bold,
//!     key_to_order(keys)
//! );
//! assert_eq!(categorical.get("foo").expect("a color"), &Srgba::new(57, 105, 172, 255));
//! ```
//!
//! A short numerical example utilizing several numerical presets:
//!
//! ```rust
//! use ratio_color::{key_to_order, ColorAtFraction, Container, LinearGradient, Numerical, Palette};
//! use palette::Srgba;
//! let keys = ["foo", "bar", "baz", "quux"];
//! let numerical: Palette<&str, LinearGradient> = Palette::with_presets(
//!     &[Numerical::SeqMagma, Numerical::SeqViridis, Numerical::CycEdge, Numerical::DivSpectral],
//!     key_to_order(keys)
//! );
//!
//! let gradient: &LinearGradient = numerical.get("bar").expect("a gradient");
//! assert_eq!(gradient, &LinearGradient::from(Numerical::SeqMagma));
//! let color: Srgba<u8> = gradient.color_at(0.5);
//! assert_eq!(color, Srgba::<u8>::new(183, 56, 120, 255));
//! ```
//!
//! The key struct of this crate is the [`Palette`] for which the [`Container`] trait has been
//! implemented. A container is a mix of a slice of values, usually a vector, and a [`BTreeMap`] of
//! keys to access them in a number of ways using the [`Selector`] enum to obtain either a suitable
//! index for the slice of values or an override value.
//!
//! In terms of palettes this corresponds to a palette having a fixed number of colors or gradients
//! to choose from (the values). When binding colors to keys, the default behavior would be to use
//! the key's order in the mapping to determine the index of the palette's color to use using
//! [`Selector::KeyOrder`]. However, you might want to use a specific color from the given palette
//! for a given key using [`Selector::Index`] or even a complete overridden value not present in the
//! usual palette [`Selector::Value`].
//!
//! The [`Palette`] is a generic struct with an implementation of [`Container`] for any orderable
//! key and any value.
//!
//!
//! # Categorical palette
//!
//! For our example palette, we will use red, green, and blue as our default colors, or values. Pink
//! will be used as a special case later on. Next we create a set of keys for which we want to have
//! colors available on demand. Just as an example, we will use "sun", "trees", "sky", "ground",
//! "ditto", and "zzz". Note that we have more keys than default colors! For some, we will assign a
//! fixed color by using an index. Others receive a color by their key's order with respect to the
//! other keys. And finally, an exception will be made for ditto.
//!
//! ```rust
//! use std::collections::BTreeMap;
//! use ratio_color::{Container, Palette, Selector};
//! use palette::Srgba;
//!
//! // Create some categorical palette colors.
//! let red: Srgba<u8> = Srgba::new(255, 0, 0, 100);
//! let green: Srgba<u8> = Srgba::new(0, 255, 0, 200);
//! let blue: Srgba<u8> = Srgba::new(0, 0, 255, 255);
//!
//! // Bundle them together in a vec to supply to the categorical palette later on.
//! let values = vec![red.clone(), green.clone(), blue.clone()];
//!
//! // Initialize a BTreeMap to map from key to a selector.
//! let mut selectors = BTreeMap::new();
//! selectors.insert("sun", Selector::Index(0)); // red
//! selectors.insert("trees", Selector::Index(1)); // green
//! selectors.insert("sky", Selector::Index(2)); // blue
//! selectors.insert("ground", Selector::KeyOrder); // what will this be?
//! selectors.insert("zzz", Selector::KeyOrder); // I'm probably last.
//!
//! // Ditto's are always pink, so they get a special value.
//! let pink: Srgba<u8> = Srgba::new(255, 125, 200, 255);
//! selectors.insert("ditto", Selector::Value(pink.clone()));
//!
//! // Create the palette.
//! let palette = Palette::new(values, selectors);
//!
//! // Let's check the contents!
//! assert_eq!(
//!     palette.get("sun").expect("a color"),
//!     &red,
//!     "our sun is red"
//! );
//! assert_eq!(
//!     palette.get("ditto").expect("a color"),
//!     &pink,
//!     "a ditto is pink"
//! );
//! assert_eq!(
//!     palette.get("ground").expect("a color"),
//!     &green,
//!     "the ground is green, because it's key is the second one by order (ditto, *ground*, sky, sun, trees, zzz)"
//! );
//! assert_eq!(
//!     palette.get("zzz").expect("a color"),
//!     &blue,
//!     "even though I'm sixth, I'll be 2 (blue) instead (index=5, per modulo 3 makes 2)"
//! )
//! ```
//!
//! So now we can safely get our colors for our given keys. The [`Palette`]'s generic nature, means
//! we can use any orderable key, and also any value. Adding or removing a key later on is done by
//! mutating the [`Palette::selectors`] property or whatever property you decide to hook up to the
//! [`Container::selectors`] trait function.
//!
//! In order to not necessarily clog your error stack, we rely on the basic behavior of maps an
//! vectors by returning values as an [`Option`] rather than throwing errors.
//!
//!
//! # Numerical palette
//!
//! Numerical palettes are completely similar to categorical palettes as far as storage and access
//! go. The [`palette`] crate no longer provides us with continuous color scales directly. As a
//! replacement, we introduce the [`ColorAtFraction`] trait and [`LinearGradient`] struct, that uses
//! the [`enterpolation`] crate to interpolate between linearized colors instead.
//!
//! ```rust
//! use palette::Srgba;
//! use ratio_color::{key_to_order, ColorAtFraction, LinearGradient, Palette, Container};
//!
//! // Create basic colors.
//! let transparent: Srgba<u8> = Srgba::new(0, 0, 0, 0);
//! let red: Srgba<u8> = Srgba::new(255, 0, 0, 255);
//! let green: Srgba<u8> = Srgba::new(0, 255, 0, 255);
//! let blue: Srgba<u8> = Srgba::new(0, 0, 255, 255);
//!
//! // Create three gradients.
//! let to_red =
//!     LinearGradient::new([transparent.clone(), red.clone()], None).expect("a gradient");
//! let to_green =
//!     LinearGradient::new([transparent.clone(), green.clone()], None).expect("a gradient");
//! let to_blue =
//!     LinearGradient::new([transparent.clone(), blue.clone()], None).expect("a gradient");
//!
//! // Get a color from a linear gradient and convert it back into a regular Srgba.
//! // Note that blueness interpolation in the linear color space does not equal regular "mean"
//! // taking, but the alpha channel does.
//! let halfway_blue = Srgba::<u8>::from_linear(to_blue.color_at(0.5));
//! assert_eq!(halfway_blue, Srgba::<u8>::new(0, 0, 188, 128));
//!
//! // Let's assert the endpoints for completeness' sake.
//! let start: Srgba<u8> = to_blue.color_at(0.0);
//! let end: Srgba<u8> = to_blue.color_at(1.0);
//! assert_eq!(&start, &transparent);
//! assert_eq!(&end, &blue);
//!
//! // Store these in an palette for these keys.
//! let keys = ["foo", "bar", "baz", "quux"];
//! // Let keys resolve to their order in the BTreeMap.
//! // Actual palette creation.
//! let palette = Palette::new(
//!     [to_red.clone(), to_green.clone(), to_blue.clone()],
//!     key_to_order(keys)
//! );
//!
//! // Test accessing and using a gradient.
//! let full_red: Srgba<u8> = palette.get("quux").expect("a gradient").color_at(1.0);
//! assert_eq!(&full_red, &red);
//! assert_eq!(
//!     <LinearGradient as ColorAtFraction<Srgba<u8>>>::color_at(
//!         palette.get("quux").expect("a gradient"),
//!         0.66
//!     ),
//!     Srgba::<u8>::new(212, 0, 0, 168),
//!         "Access quux at %66 using the fully qualified path to access the method."
//! );
//! ```

use enterpolation::linear::LinearError;
use enterpolation::{linear::Linear, Generator, Identity, Sorted};
use palette::{LinSrgba, Srgba};
use snafu::prelude::*;
use std::collections::BTreeMap;

pub mod presets;
pub use presets::{Categorical, Numerical};

/// Ratio Color error.
#[derive(Clone, Debug, Snafu)]
#[cfg_attr(feature = "serde", derive(serde::Deserialize, serde::Serialize))]
pub enum Error {
    /// Linear interpolation error.
    Linear { source: LinearError },
}

/// A selector for container contents. Either defers to a key's order or a given index of contained
/// values or an override.
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub enum Selector<Value> {
    /// Order of the key in the mapping.
    #[default]
    KeyOrder,
    /// Index of the entry to pick.
    Index(usize),
    /// An overridden value.
    Value(Value),
}

/// Create a default key to order selector mapping.
pub fn key_to_order<Key: Ord, Keys: IntoIterator<Item = Key>, Value>(
    keys: Keys,
) -> BTreeMap<Key, Selector<Value>> {
    keys.into_iter()
        .map(|key| (key, Selector::KeyOrder))
        .collect()
}

/// Anything that delivers preset values.
pub trait Preset {
    type Value;
    fn values(&self) -> Vec<Self::Value>;
}

/// A container of a slice of values (usually a vector) that are accessible by index, but also by
/// means of a mapping of keys to selectors. The selector states whether to use the key's order to
/// access the values, a given index or even a completely overridden value that is not in the list
/// of values. Values are re-used in a carousel fashion using the modulo operator for indices or
/// orders that are out of bounds.
pub trait Container {
    type Key;
    type Value;

    /// The slice of values that are used by default.
    fn values(&self) -> &[Self::Value];

    /// The number of values of the (non-override) values.
    fn len(&self) -> usize {
        self.values().len()
    }

    /// Whether the values are empty.
    fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// The mapping of key to selector. A selector is an entry's index or an override.
    fn selectors(&self) -> &BTreeMap<Self::Key, Selector<Self::Value>>;

    /// Get a value by index. Index undergoes modulo operation such that values are used in a
    /// carousel fashion by default.
    fn by_index(&self, index: usize) -> Option<&Self::Value> {
        self.values().get(index % self.len())
    }

    /// Get a value by key.
    fn get<K: Into<Self::Key>>(&self, key: K) -> Option<&Self::Value>
    where
        Self::Key: Ord,
    {
        let key: Self::Key = key.into();
        match self.selectors().get(&key) {
            Some(selector) => match selector {
                Selector::KeyOrder => self
                    .selectors()
                    .keys()
                    .position(|k| k == &key)
                    .and_then(|index| self.by_index(index)),
                Selector::Index(index) => self.by_index(*index),
                Selector::Value(value) => Some(value),
            },
            None => None,
        }
    }

    /// Create this palette from a preset.
    fn with_preset<V, P>(preset: &P, selectors: BTreeMap<Self::Key, Selector<Self::Value>>) -> Self
    where
        V: Into<Self::Value>,
        P: Preset<Value = V>;

    fn with_presets<V, P>(
        presets: &[P],
        selectors: BTreeMap<Self::Key, Selector<Self::Value>>,
    ) -> Self
    where
        V: Into<Self::Value>,
        P: Preset<Value = V>;
}

/// A palette of any kind. It stores values (often colors or gradients) that are accessible by index
/// or by key.
#[derive(Clone, Debug, Default, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct Palette<Key: Ord, Value> {
    /// Available default palette values. Used in a carousel fashion for large amounts of keys.
    /// I.e. the number of keys is greater than the number of values.
    pub values: Vec<Value>,

    /// Field keys to color selectors (indices or overrides).
    pub selectors: BTreeMap<Key, Selector<Value>>,
}
impl<Key: Ord, Value> Palette<Key, Value> {
    /// Create a new palette with values and a selectors mapping. See [`key_to_order`] to create a
    /// default mapping for keys that will map to their key's order in the map.
    pub fn new<Values>(values: Values, selectors: BTreeMap<Key, Selector<Value>>) -> Self
    where
        Values: IntoIterator<Item = Value>,
    {
        Self {
            values: values.into_iter().collect(),
            selectors,
        }
    }
}
impl<Key: Ord, Color> Container for Palette<Key, Color> {
    type Key = Key;
    type Value = Color;

    fn values(&self) -> &[Self::Value] {
        &self.values
    }

    fn selectors(&self) -> &BTreeMap<Self::Key, Selector<Self::Value>> {
        &self.selectors
    }

    fn with_preset<V, P>(preset: &P, selectors: BTreeMap<Self::Key, Selector<Self::Value>>) -> Self
    where
        V: Into<Self::Value>,
        P: Preset<Value = V>,
    {
        let values: Vec<Self::Value> = preset.values().into_iter().map(|v: V| v.into()).collect();
        Self { values, selectors }
    }

    fn with_presets<V, P>(
        presets: &[P],
        selectors: BTreeMap<Self::Key, Selector<Self::Value>>,
    ) -> Self
    where
        V: Into<Self::Value>,
        P: Preset<Value = V>,
    {
        let mut values: Vec<Self::Value> = vec![];
        for preset in presets.iter() {
            values.append(&mut preset.values().into_iter().map(|v: V| v.into()).collect());
        }
        Self { values, selectors }
    }
}

/// Capability of interpolating a color at a given fraction between 0.0 and 1.0.
pub trait ColorAtFraction<Color> {
    /// Interpolated color at a given fraction between 0.0 and 1.0.
    fn color_at(&self, fraction: f64) -> Color;
}

/// Enable a CSS gradient generation function.
pub trait CssGradient {
    /// Create a CSS gradient string with the given fractions between 0.0 and 1.0 as knots.
    fn css_gradient(&self, fractions: &[f64]) -> String;
}

/// A linearly interpolated gradient.
#[derive(Clone, Debug, PartialEq)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct LinearGradient {
    pub gradient: Linear<Sorted<Vec<f64>>, Vec<LinSrgba<f64>>, Identity>,
}
impl Default for LinearGradient {
    fn default() -> Self {
        Numerical::Ratio.into()
    }
}
impl LinearGradient {
    /// Create a new linear gradient.
    pub fn new<Color, Colors>(colors: Colors, knots: Option<&[f64]>) -> Result<Self, Error>
    where
        Color: Into<LinSrgba<f64>>,
        Colors: IntoIterator<Item = Color>,
    {
        let elements: Vec<LinSrgba<f64>> = colors.into_iter().map(|color| color.into()).collect();
        let knots = match knots {
            Some(knots) => knots.to_vec(),
            None => {
                let len = elements.len();
                let step = if len <= 1 {
                    1.0
                } else {
                    1.0 / ((len - 1) as f64)
                };
                (0..len).map(|x| x as f64 * step).collect()
            }
        };
        let linear = Linear::builder()
            .elements(elements)
            .knots(knots)
            .build()
            .with_context(|_| LinearSnafu)?;
        Ok(Self { gradient: linear })
    }
}

impl ColorAtFraction<LinSrgba<f64>> for LinearGradient {
    fn color_at(&self, fraction: f64) -> LinSrgba<f64> {
        self.gradient.gen(fraction)
    }
}

impl ColorAtFraction<Srgba<f64>> for LinearGradient {
    fn color_at(&self, fraction: f64) -> Srgba<f64> {
        Srgba::from_linear(self.gradient.gen(fraction))
    }
}

impl ColorAtFraction<Srgba<u8>> for LinearGradient {
    fn color_at(&self, fraction: f64) -> Srgba<u8> {
        Srgba::from_linear(self.gradient.gen(fraction))
    }
}

impl CssGradient for LinearGradient {
    fn css_gradient(&self, fractions: &[f64]) -> String {
        let colors: String = fractions
            .iter()
            .map(|&f| {
                let color = self.color_at(f);
                format!(
                    "{} {:.0}%",
                    to_rgba_string(Srgba::<u8>::from_linear(color)),
                    100.0 * f
                )
            })
            .collect::<Vec<_>>()
            .join(",");
        format!("linear-gradient({})", colors)
    }
}

/// Convert a Srgba (u8) color to a CSS string.
pub fn to_rgba_string(color: Srgba<u8>) -> String {
    format!(
        "rgba({},{},{},{})",
        color.red, color.green, color.blue, color.alpha
    )
}

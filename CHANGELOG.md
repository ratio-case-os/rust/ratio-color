# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.5] - 2024-01-29

### Fixed

- Updated to latest (patch) versions for dependencies.
- `LinearGradient` now just derives its `PartialEq` without falling back to `Debug`'s output for comparison.

## [0.4.4] - 2024-01-29

### Fixed

- Bump `strum` to 0.26.1 and use conventional Cargo.toml version identifiers for dependencies.

### Changed

- Implemented Ratio CASE's regular versioning approach. Since this is a utility crate that does
  not represent our "core business", we adopt a "MIT OR Apache-2.0" license.
- Switch from `anyhow` to using `snafu` as the error helper crate.

## [0.4.3] - 2023-06-21

### Fixed

- Keep back `strum` to 0.24.x since 0.25.0 introduced an error.

## [0.4.2] - 2023-06-01

### Added

- A `strum` feature that derives implementations most of [`strum`]'s traits for the preset
  enumerations. This should make iterating over the available datasets much easier when using the
  presets in UI's.

## [0.4.1] - 2023-05-26

### Added

- A `Default` implementation for `ratio_color::LinearGradient`.

## [0.4.0] - 2023-05-24

### Changed

- A near complete rewrite of the library to support a more generic interface. Both categorical and
  numerical palettes now use the same main trait/struct (Container/Palette). The key as well as the
  type of value is generic for a palette. For instance, you can create a `&str` to `palette::Srgba`
  categorical palette, and another `&str` to `ratio_color::LinearGradient` numerical palette (since
  gradients can now be interpolated by fraction).

## [0.3.2] - 2023-04-25

### Changed

- Replaced HashMap internals to BTreeMaps for it's guaranteed internal key ordering.
- Allow for newer versions of palette.

## [0.3.1] - 2023-04-13

### Fixed

- Reverted to `palette` 0.6.1 because of errors in any float multiplication. [palette#283](https://github.com/Ogeon/palette/issues/283).

## [0.3.0] - 2023-04-11

### Added

- Added easy CSS linear-gradient getter to NumericalPalette.

## [0.2.0] - 2023-04-03

### Added

- PartialEq for NumericalPalette through a implementation that uses the Debug representation of the Linear interpolator.

## [0.1.0] - 2023-04-03

### Added

- Initial version!
